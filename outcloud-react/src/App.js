import gitlab from './gitlab.png';
import formacao from './formacao.png';
import './App.css';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={formacao} className="App-title-image" alt="Formação GitLab CI/CD"/>
        <br></br>
        <img src={gitlab} className="App-logo" alt="GitLab"/>
        <p>
        Este workshop tem como objetivo principal, dar a conhecer o funcionamento de uma pipeline em GitLab conhecendo as principais Global keywords e Job keywords num ficheiro de configuração.
        </p> 
        <a className="App-link" href="https://academy.findmore.pt/ci-cd-pipelines-with-gitlab/" >
          Saiba Mais
        </a>
        <br></br>
      </header>
      <footer className="App-footer">
        <p>
        Site Versão 1  
        </p>
      </footer>
    </div>
  );
}

export default App;
